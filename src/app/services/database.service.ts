import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import ConfigConstants from '../../assets/config.js';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private http: HttpClient) {}

  getModules() {
    return this.http.get(`${ConfigConstants.baseURL}/modules`);
  }

  getModuleLeads(moduleId) {
    return this.http.get(`${ConfigConstants.baseURL}/modules/${moduleId}/leads`);
  }

  orderModules(modules) {
    const newModulesOrder = modules.map((module, index) => {
      return {...module, ordered: modules.length - index};
    });
    return this.http.put(`${ConfigConstants.baseURL}/modules/reorder`, newModulesOrder);
  }

  getModuleFormFields(moduleId) {
    return this.http.get(`${ConfigConstants.baseURL}/form_fields/${moduleId}`);
  }

  deleteFormField(moduleId) {
    return this.http.delete(`${ConfigConstants.baseURL}/form_fields/${moduleId}`);
  }

  setLead(data) {
    return this.http.post(`${ConfigConstants.baseURL}/leads`,  data);
  }

  setFormField(data) {
    return this.http.post(`${ConfigConstants.baseURL}/form_fields`,  data);
  }

  setModule(data) {
    return this.http.post(`${ConfigConstants.baseURL}/modules`,  data);
  }
}
