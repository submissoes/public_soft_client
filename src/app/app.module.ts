import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgZorroAntdModule, NZ_I18N, pt_BR } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FunilcontainerComponent } from './containers/funilcontainer/funilcontainer.component';
import {AppRoutingModule} from './app-routing.module';
import { ModulesComponent } from './containers/modules/modules.component';
import { AdicionarLeadComponent } from './components/adicionar-lead/adicionar-lead.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { AdicionarFormFieldComponent } from './components/adicionar-form-field/adicionar-form-field.component';
import { AdicionarModuloComponent } from './components/adicionar-modulo/adicionar-modulo.component';
registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FunilcontainerComponent,
    ModulesComponent,
    AdicionarLeadComponent,
    AdicionarFormFieldComponent,
    AdicionarModuloComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DragDropModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  entryComponents: [
    AdicionarLeadComponent,
    AdicionarFormFieldComponent,
    AdicionarModuloComponent
  ],
  providers: [{ provide: NZ_I18N, useValue: pt_BR }],
  bootstrap: [AppComponent]
})
export class AppModule { }
