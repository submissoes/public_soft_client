import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarLeadComponent } from './adicionar-lead.component';

describe('AdicionarLeadComponent', () => {
  let component: AdicionarLeadComponent;
  let fixture: ComponentFixture<AdicionarLeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarLeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
