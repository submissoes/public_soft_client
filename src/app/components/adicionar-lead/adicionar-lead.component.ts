import {Component, OnInit} from '@angular/core';
import {NzModalRef, NzNotificationService} from 'ng-zorro-antd';
import {DatabaseService} from '../../services/database.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-adicionar-lead',
  templateUrl: './adicionar-lead.component.html',
  styleUrls: ['./adicionar-lead.component.css']
})
export class AdicionarLeadComponent implements OnInit {

  modules: any;
  loadingFunil = true;
  loadingFormFields = false;
  loadingSendingForm = false;
  formFields: any = [];
  validateForm: FormGroup;

  constructor(private modal: NzModalRef,
              private db: DatabaseService,
              private fb: FormBuilder,
              private notification: NzNotificationService) {}

  destroyModal(): void {
    this.modal.destroy({ data: 'this the result data' });
  }

  ngOnInit(): void {
    this.loadModules();
    this.setValidateFormRules();
  }

  setValidateFormRules() {
    this.validateForm = this.fb.group({
      name: [ null, [ Validators.required ] ],
      email: [ null, [ Validators.email ] ],
      module: [ null, [ Validators.required ] ],
      surname: [ null ],
    });
  }

  loadModules() {
    this.db.getModules().subscribe(modules => {
      this.modules = modules;
      this.loadingFunil = false;
    });
  }

  submitForm() {
    this.loadingSendingForm = true;
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[ key ].markAsDirty();
      this.validateForm.controls[ key ].updateValueAndValidity();
    }
    if (this.validateForm.status === 'VALID') {
      this.db.setLead(this.validateForm.value).subscribe(
        result => {
          this.notification.create('success', 'Lead cadastrado com sucesso', '');
        },
        error => {
          this.notification.create('error', 'Houve um problema ao adicionar o lead', error.message);
        },
        () => {
          this.loadingSendingForm = false;
        }
      );
    }
  }

  removeOldControls() {
    this.formFields.forEach(field => {
      this.validateForm.removeControl(field.id);
    });
  }

  addControls(fields) {
    fields.forEach(field => {
      this.validateForm.addControl(field.id, this.fb.control(null));
    });
  }

  selectFormFields(moduleId) {
    this.loadingFormFields = true;
    this.removeOldControls();
    this.db.getModuleFormFields(moduleId).subscribe(fields => {
      this.addControls(fields);
      this.formFields = fields;
      this.loadingFormFields = false;
    });
  }

}
