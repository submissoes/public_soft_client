import { Component, OnInit } from '@angular/core';
import {NzModalRef, NzNotificationService} from 'ng-zorro-antd';
import {DatabaseService} from '../../services/database.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-adicionar-modulo',
  templateUrl: './adicionar-modulo.component.html',
  styleUrls: ['./adicionar-modulo.component.css']
})
export class AdicionarModuloComponent implements OnInit {

  validateForm: FormGroup;
  loadingSendingForm = false;


  constructor(private modal: NzModalRef,
              private db: DatabaseService,
              private fb: FormBuilder,
              private notification: NzNotificationService) {}

  ngOnInit(): void {
    this.setValidateFormRules();
  }

  setValidateFormRules() {
    this.validateForm = this.fb.group({
      name: [ null, [ Validators.required ] ],
      color: [ null, [ Validators.required ] ],
    });
  }

  submitForm() {
    this.loadingSendingForm = true;
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[ key ].markAsDirty();
      this.validateForm.controls[ key ].updateValueAndValidity();
    }
    if (this.validateForm.status === 'VALID') {
      this.db.setModule(this.validateForm.value).subscribe(
        () => {
          this.notification.create('success', 'Lead cadastrado com sucesso', '');
        },
        error => {
          this.notification.create('error', 'Houve um problema ao adicionar o lead', error.message);
        },
        () => {
          this.loadingSendingForm = false;
        });
    }
  }

}
