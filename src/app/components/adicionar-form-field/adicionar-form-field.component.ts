import { Component, Input, OnInit } from '@angular/core';
import {NzModalRef, NzNotificationService} from 'ng-zorro-antd';
import {DatabaseService} from '../../services/database.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-adicionar-form-field',
  templateUrl: './adicionar-form-field.component.html',
  styleUrls: ['./adicionar-form-field.component.css']
})
export class AdicionarFormFieldComponent implements OnInit {

  @Input() moduleId: number;
  modules: any;
  loadingSendingForm = false;
  validateForm: FormGroup;

  constructor(private modal: NzModalRef,
              private db: DatabaseService,
              private fb: FormBuilder,
              private notification: NzNotificationService) {}

  ngOnInit(): void {
    this.setValidateFormRules();
  }

  setValidateFormRules() {
    this.validateForm = this.fb.group({
      label: [ null, [ Validators.required ] ],
      type: [ null, [ Validators.required ] ],
      moduleId: [ this.moduleId, null],
    });
  }

  submitForm() {
    this.loadingSendingForm = true;
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[ key ].markAsDirty();
      this.validateForm.controls[ key ].updateValueAndValidity();
    }
    if (this.validateForm.status === 'VALID') {
      this.db.setFormField(this.validateForm.value).subscribe(
        result => {
          this.notification.create('success', 'Lead cadastrado com sucesso', '');
        },
        error => {
          this.notification.create('error', 'Houve um problema ao adicionar o lead', error.message);
        },
        () => {
          this.loadingSendingForm = false;
        });
    }
  }

}
