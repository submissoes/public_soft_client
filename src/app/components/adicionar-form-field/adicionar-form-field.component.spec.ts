import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarFormFieldComponent } from './adicionar-form-field.component';

describe('AdicionarFormFieldComponent', () => {
  let component: AdicionarFormFieldComponent;
  let fixture: ComponentFixture<AdicionarFormFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarFormFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarFormFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
