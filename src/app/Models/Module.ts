export class Module {
  id: number;
  name: string;
  color: string;
  ordered: number;
}
