import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {DatabaseService} from '../../services/database.service';
import {Module} from '../../Models/Module';
import {AdicionarFormFieldComponent} from '../../components/adicionar-form-field/adicionar-form-field.component';
import {AdicionarModuloComponent} from '../../components/adicionar-modulo/adicionar-modulo.component';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  modules: any;
  formFields: any = null;
  loadingModules = false;
  loadingFormFields = false;
  selectedModule: number;

  constructor(private db: DatabaseService,
              private notification: NzNotificationService,
              private modalService: NzModalService) { }

  ngOnInit(): void {
    this.loadModules();
  }

  loadModules(): void {
    this.loadingModules = true;
    this.db.getModules().subscribe(modules => {
      this.modules = modules;
      this.loadingModules = false;
    });
  }

  loadFormFields(moduleId): void {
    this.loadingFormFields = true;
    this.db.getModuleFormFields(moduleId).subscribe(modules => {
      this.formFields = modules;
      this.loadingFormFields = false;
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.modules, event.previousIndex, event.currentIndex);
    this.db.orderModules(this.modules).subscribe(
      result => {
        this.notification.create('success', 'Reordenado com Sucesso', '');
      },
      error => {
        this.notification.create('error', 'Houve um problema na ordenação', error.message);
      }
    );
  }

  deleteFormField(module) {
    this.db.deleteFormField(module.id).subscribe(
      result => {
        this.notification.create('success', 'Excluído com sucesso', '');
        this.loadFormFields(this.selectedModule);
      },
      error => {
        this.notification.create('error', 'Houve um problema na exclusão', error.message);
      }
    );  }

  selectModule(moduleId) {
    this.loadFormFields(moduleId);
  }

  openAdicionarFormFieldModal(): void {
    const modal = this.modalService.create({
      nzTitle: 'Adicionar Modal',
      nzContent: AdicionarFormFieldComponent,
      nzComponentParams: {
        moduleId: this.selectedModule,
      },
      nzFooter: null,
      nzOnCancel: () => this.loadFormFields(this.selectedModule)
    });
  }

  openAdicionarModule(): void {
    const modal = this.modalService.create({
      nzTitle: 'Adicionar Modal',
      nzContent: AdicionarModuloComponent,
      nzFooter: null,
      nzOnCancel: () => this.loadModules()
    });
  }
}
