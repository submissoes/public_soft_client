import {Component, OnInit} from '@angular/core';
import {DatabaseService} from '../../services/database.service';
import { NzModalService } from 'ng-zorro-antd';
import {AdicionarLeadComponent} from '../../components/adicionar-lead/adicionar-lead.component';
import {Module} from '../../Models/Module';

@Component({
  selector: 'app-funilcontainer',
  templateUrl: './funilcontainer.component.html',
  styleUrls: ['./funilcontainer.component.css']
})
export class FunilcontainerComponent implements OnInit {

  modules: any;
  leads = null;
  selectedModule: Module;
  loadingFunil = true;
  loadingLeads: boolean;

  constructor(private db: DatabaseService,
              private modalService: NzModalService) {
    this.selectedModule = new Module();
  }

  nestedTableData = [];

  ngOnInit(): void {
    this.loadModules();
  }

  loadModules(): void {
    this.db.getModules().subscribe(modules => {
      this.modules = modules;
      this.loadingFunil = false;
    });
  }

  loadLeads() {
    this.loadingLeads = true;
    this.db.getModuleLeads(this.selectedModule.id).subscribe(leads => {
      this.leads = leads;
      this.loadingLeads = false;
    });
  }

  selectModule(module) {
    this.selectedModule = module;
    this.loadLeads();
  }

  setBlockStyle(module) {
    return {
      width: `${module.ordered * 5}em`,
      borderTopColor: module.color || 'grey'
    };
  }

  openAdicionarLeadModa(): void {
    const modal = this.modalService.create({
      nzTitle: 'Adicionar Modal',
      nzContent: AdicionarLeadComponent,
      nzFooter: null
    });
  }

}
