import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunilcontainerComponent } from './funilcontainer.component';

describe('FunilcontainerComponent', () => {
  let component: FunilcontainerComponent;
  let fixture: ComponentFixture<FunilcontainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunilcontainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunilcontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
