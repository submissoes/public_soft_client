import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ModulesComponent} from './containers/modules/modules.component';
import {FunilcontainerComponent} from './containers/funilcontainer/funilcontainer.component';

const routes: Routes = [
  {
    path: '',
    component:  FunilcontainerComponent
  },
  {
    path: 'modules',
    component:  ModulesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {

}
